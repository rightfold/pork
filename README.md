# Installation

To run Pork, you first have to compile the interpreter. All you need is a C++
compiler.

Then just invoke it with `video-game.bef` as argument:

    $ ./a.out video-game.bef

# Usage

You are responsible for a herd of pigs. When the herd is gone, you lose.

When a pig tries to escape, you can enter `c` to attempt to catch it.
