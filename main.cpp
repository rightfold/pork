#include <algorithm>
#include <array>
#include <cassert>
#include <fstream>
#include <iostream>
#include <random>
#include <string>
#include <vector>

enum class direction_t { north, south, west, east } direction = direction_t::east;
std::array<std::array<int, 80>, 25> program;
std::vector<int> stack;
int x, y;

void next() {
    switch (direction) {
        case direction_t::north: --y; break;
        case direction_t::south: ++y; break;
        case direction_t::west: --x; break;
        case direction_t::east: ++x; break;
    }

    if (x < 0) x = 79;
    if (x > 79) x = 0;
    if (y < 0) y = 24;
    if (y > 24) y = 0;
}

int pop() {
    if (stack.empty())
        return 0;

    auto result = stack.back();
    stack.pop_back();
    return result;
}

void push(int x) {
    stack.push_back(x);
}

int main(int argc, char const** argv) {
    assert(argc == 2);

    std::cout.rdbuf()->pubsetbuf(0, 0);

    for (auto&& row : program) {
        for (auto&& column : row) {
            column = ' ';
        }
    }

    {
        std::ifstream stream(argv[1]);
        for (auto&& row : program) {
            std::string line;
            if (!std::getline(stream, line))
                break;
            std::copy(line.begin(), std::min(line.begin() + 80, line.end()),
                      row.begin());
        }
    }

    bool string_mode = false;
    for (;;) {
        if (string_mode) {
            if (program[y][x] == '"')
                string_mode = false;
            else
                push(program[y][x]);
            next();
            continue;
        }

        switch (program[y][x]) {
            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                push(program[y][x] - '0');
                next();
                break;

            case '+':
                push(pop() + pop());
                next();
                break;

            case '-': {
                auto a = pop();
                auto b = pop();
                push(b - a);
                next();
                break;
            }

            case '*':
                push(pop() * pop());
                next();
                break;

            case '/': {
                auto a = pop();
                auto b = pop();
                while (a == 0) std::cin >> a;
                push(b / a);
                next();
                break;
            }

            case '%': {
                auto a = pop();
                auto b = pop();
                while (a == 0) std::cin >> a;
                push(b % a);
                next();
                break;
            }

            case '!':
                push(!pop());
                next();
                break;

            case '`': {
                auto a = pop();
                auto b = pop();
                push(b > a);
                next();
                break;
            }

            case '>':
                direction = direction_t::east;
                next();
                break;

            case '<':
                direction = direction_t::west;
                next();
                break;

            case '^':
                direction = direction_t::north;
                next();
                break;

            case 'v':
                direction = direction_t::south;
                next();
                break;

            case '?': {
                static std::mt19937 prng((std::random_device()()));
                static std::uniform_int_distribution<> dist(0, 3);
                direction = static_cast<direction_t>(dist(prng));
                next();
                break;
            }

            case '_':
                if (pop())
                    direction = direction_t::west;
                else
                    direction = direction_t::east;
                next();
                break;

            case '|':
                if (pop())
                    direction = direction_t::south;
                else
                    direction = direction_t::north;
                next();
                break;

            case '"':
                string_mode = true;
                next();
                break;

            case ':': {
                auto a = pop();
                push(a);
                push(a);
                next();
                break;
            }

            case '\\': {
                auto a = pop();
                auto b = pop();
                push(a);
                push(b);
                next();
                break;
            }

            case '$':
                pop();
                next();
                break;

            case '.':
                std::cout << pop();
                next();
                break;

            case ',':
                std::cout << static_cast<char>(pop());
                next();
                break;

            case '#':
                next();
                next();
                break;

            case 'p': {
                auto y_ = pop();
                auto x_ = pop();
                auto v = pop();
                program[y_][x_] = v;
                next();
                break;
            }

            case 'g': {
                auto y_ = pop();
                auto x_ = pop();
                push(program[y_][x_]);
                next();
                break;
            }

            case '&': {
                int v;
                std::cin >> v;
                push(v);
                next();
                break;
            }

            case '~': {
                char v;
                std::cin >> v;
                push(v);
                next();
                break;
            }

            case '@':
                return 0;

            case ' ':
                next();
                break;

            default:
                return -1;
        }
    }
}
